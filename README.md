Doutor em Markdown
==================


Caso esteja prestes a escrever sua  dissertação ou tese, odeia o Word, tem pouca 
familiaridade ou nenhum conhecimento sobre [LaTeX],
**Doutor em Markdown** é a solução. Aqui você encontra um modelo de trabalho acadêmico 
dentro das normas da ABNT que usufrui dos benefícios do [LaTeX], mas que usa o 
[Markdown](https://pt.wikipedia.org/wiki/Markdown) como sintaxe básica para a formatação
do texto.

O modelo em uso se deve à equipe do [abnTeX](https://www.abntex.net.br/). 
Mais precisamente, esse modelo se baseia na adaptação da suíte **abnTeX** para uso
com o conversor de documentos [Pandoc].


Requisitos
----------

Para usar o modelo você precisará

1. Ter o LaTeX instalado no computador
2. Instalar o [Pandoc]
3. Instalar o `pandoc-citeproc`, uma extensão do [Pandoc] para manipular referências 
bibliográficas.


Usando o Modelo
---------------

A estrutra desse repositório é a seguinte.

```
doutor-em-markdown
|-- 0_abstract.txt *
|-- 0_prefacio.txt *
|-- 0_resumo.txt *
|-- Cap_01.txt
|-- Cap_02.txt
|-- Cap_03.txt
|-- Cap_04.txt
|-- compilar *
|-- Conclusao.txt
|-- config **
|   |-- abntex2.tex
|   |-- babilonia.bib
|   |-- formato-citacao.csl
|   `-- metadata.yaml
|-- img **
|   `-- tux.png
|-- referencias.txt *
`-- thesis.pdf
```

Os arquivos cujo nome é sucedido por um `*` podem ser renomeados como você queira, mas
_não_ podem ser deletados nem ter seus códigos em **LaTeX** alterados, a menos que saiba
o que está fazendo.

Os diretórios/pastas com `**` não podem ser deletados nem renomeados, pois possuem arquivos de configuração.

Os demais arquivos podem ser apagados, sem problemas.

O arquivo `Cap_01.txt` contém uma breve introdução ao [Markdown] que você pode achar útil. (O tutorial ainda está em construção)


### Primeiros passos


Comece alterando as informações presentes no arquivo `metadata.yaml`. Em seguida escreva alguma coisa nos arquivos `Cap_02.txt`, `Cap_03.txt`, ...

Com o pandoc instalado, agora você compilar sua tese. Utilize o seguinte comando.

```
pandoc ./config/metadata.yaml --filter pandoc-citeproc --template ./config/abntex2.tex --pdf-engine=pdflatex -N --top-level-division=chapter *.txt -o thesis.pdf
```

Pronto! Agora você deve ter sua tese, ou ao menos pode focar apenas em escrever e deixar
a formatação por conta do modelo.

- TODO: explicação dos comandos

Certamente você deve ter achado o comando acima muito grande. Por conta disso, para usuários Linux há o arquivo `compilar` que é um script para compilar o arquivo. Basta executar

```
$ ./compilar *.txt
```

Caso não queira compilar todos os arquivos, basta dar um nome apenas. Por exemplo,


```
$ ./compilar Cap_02.txt
```


TODO
----

- explicação do arquivos `metadados.yaml`
- explicação do uso de referências bibliográficas
- o que houver



[LaTeX]: https://pt.wikipedia.org/wiki/LaTeX
[Pandoc]: https://pandoc.org/
